import java.util.Scanner;

public class Mittelwert {

   public static void main(String[] args) {
	   Scanner scan = new Scanner (System.in);
      // (E) "Eingabe"
      // Werte für x und y festlegen:
      // ===========================
	   System.out.println("Wie viele Werte wollen sie eingeben? ");
	  int anzahlwerte = scan.nextInt();
	  double akumulator = 0;
	  double [] zahlen = new double [anzahlwerte];
	
      double m;
      
      for (int i = 0; i <anzahlwerte;i++){
    	  System.out.print("Bitte geben Sie einen Wert ein ");
    	  zahlen[i] = scan.nextDouble();
    	  akumulator = zahlen[i];
      }
    		  
      
      // (V) Verarbeitung
    		  
      // Mittelwert von x und y berechnen: 
      // ================================
      m = akumulator / anzahlwerte;
      
      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      // =================================
      System.out.printf("Der Mittelwert von %.2f\n",m);
      for ( int i = 0 ; i <zahlen.length; i++) {
    		System.out.print(zahlen[i] +" ; ");  

   }
   }
}

